drop database testeSQL;
CREATE database testeSQL;
use testeSQL;

CREATE TABLE produto (
	id_produto int NOT NULL AUTO_INCREMENT,
	nome_produto varchar(50) NOT NULL,
    valor_produto varchar(50) NOT NULL,
	PRIMARY KEY (id_produto)
);

CREATE TABLE pedido (
	id_pedido int NOT NULL AUTO_INCREMENT,
	nome_cliente_pedido varchar(50) NOT NULL,
    data_hora_pedido datetime NOT NULL,
    valor_pedido varchar(50) NOT NULL,
    id_produto INT NOT NULL,
    quantia_produto_pedido varchar(50) NOT NULL,
	PRIMARY KEY (id_pedido),
    FOREIGN KEY (id_produto) REFERENCES produto(id_produto)
);

INSERT INTO produto (nome_produto, valor_produto)
VALUES('Teclado','127.99');

INSERT INTO produto (nome_produto, valor_produto)
VALUES('Mouse','120.99');

INSERT INTO produto (nome_produto, valor_produto)
VALUES('Fone','130.25');

INSERT INTO produto (nome_produto, valor_produto)
VALUES('Cadeira','20');

UPDATE produto
SET valor_produto = '199.99'
WHERE nome_produto = 'Teclado';

DELETE FROM produto
WHERE id_produto = 1;

INSERT INTO pedido (nome_cliente_pedido, data_hora_pedido, valor_pedido, id_produto, quantia_produto_pedido)
VALUES('Gabriel Muhlstedt', CURRENT_TIMESTAMP(), (SELECT (5 * CAST(valor_produto AS DECIMAL(10,2))) FROM produto WHERE id_produto = 2), 2, '5');

INSERT INTO pedido (nome_cliente_pedido, data_hora_pedido, valor_pedido, id_produto, quantia_produto_pedido)
VALUES('Geancarlo Dahle', CURRENT_TIMESTAMP(), (SELECT (7 * CAST(valor_produto AS DECIMAL(10,2))) FROM produto WHERE id_produto = 3), 3, '7');

INSERT INTO pedido (nome_cliente_pedido, data_hora_pedido, valor_pedido, id_produto, quantia_produto_pedido)
VALUES('Giselle cavalli', CURRENT_TIMESTAMP(), (SELECT (10 * CAST(valor_produto AS DECIMAL(10,2))) FROM produto WHERE id_produto = 4), 4, '10');

UPDATE pedido
SET data_hora_pedido = '2022-10-10 18:23:10.000'
WHERE id_pedido = 1;

DELETE FROM pedido
WHERE id_pedido = 1;

SELECT * FROM pedido
ORDER BY data_hora_pedido DESC;

SELECT * FROM produto
ORDER BY valor_produto ASC;