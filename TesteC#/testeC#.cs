using System;

namespace DescontoBlackFriday
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Informe o valor do produto:");
            Console.WriteLine("Exemplo Valor: 2425.99");
            decimal valorProduto = decimal.Parse(Console.ReadLine());

            decimal valorProdutoDesconto = valorProduto * 0.6m;
            decimal valorDesconto = valorProduto - valorProdutoDesconto;

            Console.WriteLine($"Produto custava: {valorProduto}");
            Console.WriteLine($"Com o desconto: {valorProdutoDesconto}");
            Console.WriteLine($"Valor do desconto: {valorDesconto}");

            Console.ReadLine();
        }
    }
}