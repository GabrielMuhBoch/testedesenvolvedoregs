drop database testeFrontEnd;
CREATE database testeFrontEnd;
use testeFrontEnd;


CREATE TABLE users (
	id_user int NOT NULL AUTO_INCREMENT,
	nome_user varchar(50) NOT NULL,
    sobrenome_user varchar(50) NOT NULL,
	birth_date_user date,
	email_user varchar(50) NOT NULL,
	cpf_user varchar(14),
	PRIMARY KEY (id_user)
);

INSERT INTO users (nome_user, sobrenome_user, birth_date_user, email_user, cpf_user) 
VALUES('Gabriel', 'Muhlstedt' ,'10/10/2002' , 'teste@gmail.com' , '11100033366');
