const express = require("express");
const cors = require("cors");
const morgan = require("morgan")
const bodyParser = require("body-parser");
const createError = require('http-errors');
const router = require('./router');

const app = express();

app.use(morgan('dev'))
app.use(cors());
app.use(bodyParser.json());

//Router
app.use(router);

// 404
app.use((req, res, next) => {
  next(createError(404, 'Rota não encontrada'));
});

// Erros
app.use((err, req, res, next) => {
  const status = err.status || 500;
  const message = 'Ocorreu um erro. Tente novamente mais tarde.';
  const error = err.message || 'ERRO GENÉRICO: Não mapeado.'

  console.log(error);
  
  res.status(status).json({
    success: false,
    message,
    error: error
  });
});

module.exports = app;
