const UsersModel = require('../models/UsersModel');
const createError = require('http-errors');

class UsersController {

    async cadastrar(req, res, next) {
        const nome = req.body.nome;
        const sobrenome = req.body.sobrenome;
        const dataNascimento = req.body.idade;
        const email = req.body.email;
        const cpf = req.body.cpf;

        const require_fields = (!req.body.nome && !req.body.sobrenome && !req.body.dataNascimento && !req.body.email && !req.body.cpf);

        const params = { "nome": nome, "sobrenome": sobrenome, "dataNascimento": dataNascimento, "email": email, "cpf": cpf }
        console.log(params);

        if (!require_fields) {
            UsersModel.cadastrar(params, function (err, result) {
                console.log(result);
                console.log(err);
                if (err) {
                    console.log('Não foi possivel concluir a ação.', err);
                    next(createError(400, err));
                } else {
                    res.status(200).send({ status: 200, message: 'Cadastro Realizado com sucesso.', success: true });
                }
            });
        }else{
            next(createError(400, "Campo com problema"));
        }
    }

    async get_all(req, res, next) {
        UsersModel.get_all_users(function (err, result) {
            if (err) {
                console.log('Erro ao listar usuarios:', err);
                next(createError(400, err));
            } else {
                res.status(200).send({ status: 200, data: result,message: 'Listagem Realizada com sucesso.', success: true });
            }
        })
    }


}

module.exports = new UsersController();
