const swaggerAutogen = require('swagger-autogen')()
require('dotenv').config();

const doc = {
    info: {
        title: 'TesteFrontEnd',
        host: process.env.BASE_URL
    },
    host: process.env.BASE_URL
  };

const outputFile = './src/middleware/swagger/swagger_output.json'
const endpointsFiles = ['src/app.js']

swaggerAutogen(outputFile, endpointsFiles, doc).then(() => {
    require('../../../server')
})
