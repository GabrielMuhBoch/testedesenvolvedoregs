const connection = require('../middleware/mysql/connection_db')

function cadastrar(req, callback) {
    connection.query(`INSERT INTO users (nome_user, sobrenome_user, birth_date_user, email_user, cpf_user) 
    VALUES('${req.nome}', '${req.sobrenome}' ,'${req.dataNascimento}' , '${req.email}' , '${req.cpf}')`, function (err, results, fields) {
            if (err) {
                return callback(err, null);
            }
            return callback(null, results);
        });
}

function get_all_users(callback) {
    connection.query(`SELECT * FROM users;`, function (err, results, fields) {
        if (err) {
            return callback(err, null);
        }
        console.log(results);
        return callback(null, results);

    });
}

//exporta as funções para serem utilizadas em outros arquivos
module.exports = {
    cadastrar,
    get_all_users
};
