const express = require('express');
const router = express.Router();

const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./middleware/swagger/swagger_output.json');

// Routes
const usersRoutes = require('./routes/UsersRoutes');
router.use('/users', usersRoutes);

// Doc Swagger
router.use('/doc', swaggerUi.serve);
router.get('/doc', swaggerUi.setup(swaggerDocument) /* #swagger.ignore = true*/);

module.exports = router;
