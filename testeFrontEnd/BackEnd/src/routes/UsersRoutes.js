const express = require("express");
const router = express.Router();
const UsersController = require("../controllers/UsersController");


router.post("/cadastro", UsersController.cadastrar /* #swagger.tags = ['Users']*/);
router.get("/usuarios", UsersController.get_all /* #swagger.tags = ['Users']*/);

module.exports = router;