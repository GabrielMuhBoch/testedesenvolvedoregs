import { createRouter, createWebHistory } from 'vue-router'
import Cadastro from '../views/CadastroView.vue'
import Tabela from '../views/TabelaView.vue'

const routes = [
  {
    path: '',
    component: Cadastro
  },
  {
    path: '/tabela',
    component: Tabela
  }
  
]

const router = createRouter({
  history: createWebHistory(),
  routes
})

export default router
